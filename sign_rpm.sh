#!/bin/bash
# Usage: . ./sign_rpm.sh glob

# Sign RPM files
echo -e "%_signature gpg" >> $HOME/.rpmmacros
echo -e "%_gpg_path /root/.gnupg" >> $HOME/.rpmmacros
echo -e "%_gpg_name $GPG_NAME" >> $HOME/.rpmmacros
echo -e "%_gpgbin /usr/bin/gpg" >> $HOME/.rpmmacros
echo -e "%_gpg_digest_algo sha256" >> $HOME/.rpmmacros

echo "$GPG_PUBLIC_KEY" > /tmp/public
echo "$GPG_PRIVATE_KEY" > /tmp/private
gpg --allow-secret-key-import --import /tmp/private || true
rpm --import /tmp/public || true

for f in $@
do
  echo "GPG signing $f..."
expect <(cat <<EOD
spawn rpm --resign $f
expect -exact "Enter pass phrase: "
send -- "$GPG_PASS_PHRASE\r"
expect eof
EOD
)
done

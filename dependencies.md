# wine32 dependency mapping

The following graph shows the dependencies for building and running wine32. Only
packages that are **not** in the standard CentOS base repos are shown. All required packages are rebuilt for i386/i686 from the EPEL source RPMs.

## Key

```
7.2: Requires CentOS 7.2 to build
7.6: Requires CentOS 7.6 to build
br:  BuildRequires
r:   Requires
ubr: Undocumented BuildRequires
```

## Graph

```mermaid
graph TD

cmake3[cmake3 7.6]
dbus-cplusplus-devel[dbus-c++-devel 7.2]
epel-rpm-macros[epel-rpm-macros noarch]
gtest-devel[gtest-devel 7.6]
jack-audio-connection-kit-devel[jack-audio-connection-kit-devel 7.6]
jsoncpp-devel[jsoncpp-devel 7.6]
libffado-devel[libffado-devel 7.2]
libuv-devel[libuv-devel 7.6]
libvkd3d-devel[libvkd3d-devel 7.6]
libxml++-devel[libxml++-devel 7.2]
libzstd-devel[libzstd-devel 7.6]
openal-soft-devel[openal-soft-devel 7.6]
perl-generators[perl-generators noarch]
portaudio-devel[portaudio-devel 7.6]
python2-scons[python2-scons noarch]
python2-rpm-macros[python2-rpm-macros noarch]
python36-devel[python36-devel 7.6]
python3-rpm-macros[python3-rpm-macros noarch]
python-rpm-macros[python-rpm-macros noarch]
python-srpm-macros[python-srpm-macros noarch]
ninja-build[ninja-build 7.6]
nss-mdns[nss-mdns 7.6]
re2c[re2c 7.6]
rhash-devel[rhash-devel 7.6]
SDL2-devel[SDL2-devel 7.6]
spirv-headers-devel[spirv-headers-devel noarch]
spirv-tools-devel[spirv-tools-devel 7.6]
wine[wine 7.6]

wine --> |r| nss-mdns
wine --> |br| libvkd3d-devel
wine --> |br| openal-soft-devel
wine --> |br| perl-generators
wine --> |br| SDL2-devel

libvkd3d-devel --> |br| spirv-headers-devel
libvkd3d-devel --> |br| spirv-tools-devel

spirv-tools-devel --> |br| cmake3
spirv-tools-devel --> |br| ninja-build
spirv-tools-devel --> |br| python3-rpm-macros
spirv-tools-devel --> |br| python36-devel
spirv-tools-devel --> |br| spirv-headers-devel

cmake3 --> |br| jsoncpp-devel
cmake3 --> |br| libuv-devel
cmake3 --> |br| python36-devel
cmake3 --> |br| rhash-devel
cmake3 --> |br| libzstd-devel

libuv-devel --> |ubr| epel-rpm-macros

epel-rpm-macros --> |r| python-rpm-macros 
epel-rpm-macros --> |r| python-srpm-macros
epel-rpm-macros --> |r| python2-rpm-macros

ninja-build --> |br| gtest-devel
ninja-build --> |br| re2c

python36-devel --> |br| python-rpm-macros

python-rpm-macros --> |r| python-srpm-macros

openal-soft-devel --> |br| portaudio-devel

portaudio-devel --> |br| jack-audio-connection-kit-devel

jack-audio-connection-kit-devel --> |br| libffado-devel

libffado-devel --> |br| dbus-cplusplus-devel
libffado-devel --> |br| libxml++-devel
libffado-devel --> |br| python2-scons
```